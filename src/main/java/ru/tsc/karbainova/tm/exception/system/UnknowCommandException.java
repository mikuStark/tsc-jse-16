package ru.tsc.karbainova.tm.exception.system;

import ru.tsc.karbainova.tm.exception.AbstractException;

public class UnknowCommandException extends AbstractException {
    public UnknowCommandException() {
        super("Incorrect command.");
    }
}
