package ru.tsc.karbainova.tm.controller;

import ru.tsc.karbainova.tm.api.controller.ICommandController;
import ru.tsc.karbainova.tm.api.service.ICommandService;
import ru.tsc.karbainova.tm.model.Command;
import ru.tsc.karbainova.tm.util.NumberUtil;

public class CommandController implements ICommandController {

    private final ICommandService commandService;

    public CommandController(ICommandService commandService) {
        this.commandService = commandService;
    }

    @Override
    public void displayWelcome() {
        System.out.println("** WELCOME TO TASK MANAGER **");
    }

    @Override
    public void exit() {
        System.exit(0);
    }

    @Override
    public void showInfo() {
        final int processors = Runtime.getRuntime().availableProcessors();
        System.out.println("Available processors: " + processors);
        final long freeMemory = Runtime.getRuntime().freeMemory();
        System.out.println("Free memory: " + NumberUtil.formatBytes(freeMemory));
        final long maxMemory = Runtime.getRuntime().maxMemory();
        final String maxMemoryFormat = NumberUtil.formatBytes(maxMemory);
        final String maxMemoryValue = (maxMemory == Long.MAX_VALUE) ? "no limit" : maxMemoryFormat;
        System.out.println("Maximum memory: " + maxMemoryValue);
        final long totalMemory = Runtime.getRuntime().totalMemory();
        System.out.println("Total memory available to JVM: " + NumberUtil.formatBytes(totalMemory));
        final long usedMemory = totalMemory - freeMemory;
        System.out.println("Used memory by JVM: " + NumberUtil.formatBytes(usedMemory));
    }

    @Override
    public void displayHelp() {
        Command[] commands = commandService.getTerminalCommands();
        for (final Command command : commands) {
            System.out.println(command);
        }
    }

    @Override
    public void showCommands() {
        Command[] commands = commandService.getTerminalCommands();
        for (final Command command : commands) {
            showCommandValue(command.getName());
        }
    }

    @Override
    public void showArguments() {
        Command[] commands = commandService.getTerminalCommands();
        for (final Command command : commands) {
            showCommandValue(command.getArgument());
        }
    }

    @Override
    public void showCommandValue(String value) {
        if (value == null || value.isEmpty()) return;
        System.out.println(value);
    }

    @Override
    public void displayVersion() {
        System.out.println("1.0.0");
    }

    @Override
    public void displayAbout() {
        System.out.println("Developer: Mariya Karbainova");
        System.out.println("E-mail: mariya@karbainova");
    }

    @Override
    public void displayErrorCommand() {
        System.err.println("Error command!!! Command not found...");
    }

    @Override
    public void displayErrorArg() {
        System.err.println("Error argument!!! Argument not found...");
    }
}
