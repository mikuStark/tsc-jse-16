package ru.tsc.karbainova.tm.constant;

public class TerminalConst {
    public static final String CMD_HELP = "help";
    public static final String CMD_VERSION = "version";
    public static final String CMD_ABOUT = "about";
    public static final String CMD_EXIT = "exit";
    public static final String CMD_INFO = "info";
    public static final String CMD_COMMANDS = "commands";
    public static final String CMD_ARGUMENTS = "arguments";
    public static final String CREATE_PROJECT = "create-project";
    public static final String LIST_PROJECT = "list-project";
    public static final String CLEAR_PROJECT = "clear-project";
    public static final String CREATE_TASK = "create-task";
    public static final String LIST_TASK = "list-task";
    public static final String CLEAR_TASK = "clear-task";

    public static final String FIND_BY_ID_PROJECT = "find-by-id-project";
    public static final String FIND_BY_INDEX_PROJECT = "find-by-index-project";
    public static final String FIND_BY_NAME_PROJECT = "find-by-name-project";
    public static final String REMOVE_BY_ID_PROJECT = "remove-by-id-project";
    public static final String REMOVE_BY_INDEX_PROJECT = "remove-by-index-project";
    public static final String REMOVE_BY_NAME_PROJECT = "remove-by-name-project";
    public static final String UPDATE_BY_ID_PROJECT = "update-by-id-project";
    public static final String UPDATE_BY_INDEX_PROJECT = "update-by-index-project";

    public static final String STATUS_START_BY_ID_PROJECT = "status-start-by-id-project";
    public static final String STATUS_START_BY_INDEX_PROJECT = "status-start-by-index-project";
    public static final String STATUS_START_BY_NAME_PROJECT = "status-start-by-name-project";
    public static final String STATUS_FINISH_BY_ID_PROJECT = "status-finish-by-id-project";
    public static final String STATUS_FINISH_BY_INDEX_PROJECT = "status-finish-by-index-project";
    public static final String STATUS_FINISH_BY_NAME_PROJECT = "status-finish-by-name-project";

    public static final String FIND_BY_ID_TASK = "find-by-id-task";
    public static final String FIND_BY_INDEX_TASK = "find-by-index-task";
    public static final String FIND_BY_NAME_TASK = "find-by-name-task";
    public static final String REMOVE_BY_ID_TASK = "remove-by-id-task";
    public static final String REMOVE_BY_INDEX_TASK = "remove-by-index-task";
    public static final String REMOVE_BY_NAME_TASK = "remove-by-name-task";
    public static final String UPDATE_BY_ID_TASK = "update-by-id-task";
    public static final String UPDATE_BY_INDEX_TASK = "update-by-index-task";

    public static final String STATUS_START_BY_ID_TASK = "status-start-by-id-task";
    public static final String STATUS_START_BY_INDEX_TASK = "status-start-by-index-task";
    public static final String STATUS_START_BY_NAME_TASK = "status-start-by-name-task";
    public static final String STATUS_FINISH_BY_ID_TASK = "status-finish-by-id-task";
    public static final String STATUS_FINISH_BY_INDEX_TASK = "status-finish-by-index-task";
    public static final String STATUS_FINISH_BY_NAME_TASK = "status-finish-by-name-task";

    public static final String FIND_ALL_TASK_BY_PROJECT_ID = "find-all-task-by-project-id";
    public static final String BIND_TASK_TO_PROJECT_BY_ID = "bind-task-to-project-by-id";
    public static final String TASK_UNBIND_BY_ID = "task-unbind-by-id";

}
