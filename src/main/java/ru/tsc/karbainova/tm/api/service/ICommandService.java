package ru.tsc.karbainova.tm.api.service;

import ru.tsc.karbainova.tm.model.Command;

public interface ICommandService {
    Command[] getTerminalCommands();
}
