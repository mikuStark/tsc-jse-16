package ru.tsc.karbainova.tm.api.service;

import ru.tsc.karbainova.tm.model.Task;

import java.util.Comparator;
import java.util.List;

public interface ITaskService {

    void create(String name);

    void create(String name, String description);

    void add(Task task);

    void remove(Task task);

    List<Task> findAll();

    List<Task> findAll(Comparator<Task> comparator);

    void clear();

    Task updateById(String id, String name, String description);

    Task updateByIndex(Integer index, String name, String description);

    Task removeById(String id);

    Task removeByIndex(Integer index);

    Task removeByName(String name);

    Task findById(String id);

    Task findByIndex(Integer index);

    Task findByName(String name);

    Task startById(String id);

    Task startByIndex(Integer index);

    Task startByName(String name);

    Task finishById(String id);

    Task finishByIndex(Integer index);

    Task finishByName(String name);
}
